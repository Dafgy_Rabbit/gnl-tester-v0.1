/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arykov <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/20 16:09:38 by arykov            #+#    #+#             */
/*   Updated: 2018/01/17 19:52:32 by arykov           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		a;
	unsigned const char	*str;

	a = (unsigned char)c;
	str = (unsigned char*)s;
	while (n--)
	{
		if (*str == a)
			return ((void *)str);
		str++;
	}
	return (NULL);
}
