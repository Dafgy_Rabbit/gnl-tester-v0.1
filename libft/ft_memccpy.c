/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arykov <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/20 16:01:33 by arykov            #+#    #+#             */
/*   Updated: 2017/12/20 16:02:09 by arykov           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char			*d;
	const unsigned char		*sorc;
	int						i;

	d = (unsigned char*)dst;
	sorc = (unsigned char*)src;
	i = 0;
	while (i < (int)n)
	{
		d[i] = sorc[i];
		if (sorc[i++] == (unsigned char)c)
			return (&dst[i]);
	}
	return (NULL);
}
