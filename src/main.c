#include "get_next_line.h"
#include <stdio.h>

int		main(int argc, char **argv)
{
	int o;
	int mfd;
	int i;
	int fd;
	int fd2;
	int fd3;
	char *line;

	if (argc < 2 || argc > 4)
	{
		printf("Wrong input!\n");
		return (0);
	}
	fd = open(argv[1], O_RDONLY);
	fd2 = open(argv[2], O_RDONLY);
	fd3 = open(argv[3], O_RDONLY);
	mfd = 0;
	o = 0;
	i = 1;
	printf("\nfd = %d fd2 = %d fd3 = %d\n", fd, fd2, fd3);
	while (o > -1)
	{
		if (fd > -1 && mfd == 0)
		{
			if (get_next_line(fd, &line) == 0)
				fd = -1;
			else
				printf("%d(%d). \"%s\"\n", i, fd, line);
			mfd = 1;
		}
		else if (fd2 > -1 && mfd == 1)
		{
			if (get_next_line(fd2, &line) == 0)
				fd2 = -1;
			else
				printf("%d(%d). \"%s\"\n", i, fd2, line);
			mfd = 2;
		}
		else if (fd3 > -1 && mfd == 2)
		{
			if (get_next_line(fd3, &line) == 0)
				fd3 = -1;
			else
				printf("%d(%d). \"%s\"\n", i, fd3, line);
			mfd = 0;
		}
		if (fd == -1 && fd2 == -1 && fd3 == -1)
			break;
		else if (fd == -1 && mfd == 0)
			mfd = 1;
		else if (fd2 == -1 && mfd == 1)
			mfd = 2;
		else if (fd3 == -1 && mfd == 2)
			mfd = 0;
		i++;
	}
	return (0);
}